export var getters={
	getLastResult: state => {
		return state.lastResult;
	},
	getHistory: state => {
		return state.historyItems;
	},
	getMnistData: state => {
		return state.mnistData;
	},
	getModelWeights: state => {
		return state.modelWeights;
	}
}