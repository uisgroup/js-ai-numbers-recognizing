<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

if(!empty($_FILES)){
    $uploaddir = '/var/www/ai/';

    try {
        rename("model.json",
            "lastModels/model_".date('d.m.Y H:i:s').".json");
        rename("model.weights.bin",
            "lastModels/model.weights_".date('d.m.Y H:i:s').".bin");
    }catch (Exception $e){

    }
    echo '<pre>';
    if(!empty($_FILES['model_json']) && !empty($_FILES['model_weights_bin'])) {
        $uploadfile = $uploaddir . basename($_FILES['model_json']['name']);
        echo $uploadfile."\n";
        if (move_uploaded_file($_FILES['model_json']['tmp_name'], $uploadfile)) {
            chmod($uploadfile, 0777);
            echo "Файл model_json корректен и был успешно загружен.\n";
        }
        $uploadfile = $uploaddir . basename($_FILES['model_weights_bin']['name']);
        echo $uploadfile."\n";
        if (move_uploaded_file($_FILES['model_weights_bin']['tmp_name'], $uploadfile)) {
            chmod($uploadfile, 0777);
            echo "Файл model_weights_bin корректен и был успешно загружен.\n";
        }
    }
    echo 'Некоторая отладочная информация:';
    print_r($_FILES);

    print "</pre>";
}

echo 'ok';